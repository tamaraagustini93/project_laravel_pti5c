@extends('layout.layoutabout')

@section('main')
    
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>ABOUT <span>ME</span></h1>
    <span class="title-bg">Resume</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-12 col-lg-5 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">PERSONAL INFO</h3>
                        
                        <p class="text-uppercase pb-5 mb-0 text-left text-sm-center">Tamara agustini, Mahasiswa universitas pendidikan ganesha dari program studi pendidikan teknik informatika semester 5 </p>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/tamara1.jpg" class="img-fluid main-img-mobile" alt="my picture" />
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">first name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Ni Kadek Tamara</span> </li>
                            <li> <span class="title">last name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Agustini</span> </li>
                            <li> <span class="title">Age :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20 Years</span> </li>
                            <li> <span class="title">Nationality :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                            <li> <span class="title">Address :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Karangasem</span> </li>
                            <li> <span class="title">phone :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">085935328544</span> </li>
                            <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">tamara@undiksha.ac.id</span> </li>
                            <li> <span class="title">langages :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia, English</span> </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                        <img src="img/tamara1.jpg" width="350px" alt="myprofil">    
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Personal Info Ends -->
            
        </div>

        
        <hr class="separator mt-1">
        <!-- Experience & Education Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Experience <span>&</span> Education</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2021 - 2022</span>
                            <h5 class="poppins-font text-uppercase">Pengurus BEM FTK UNDIKSHA <span class="place open-sans-font">Ganesha University Of Education</span></h5>
                            <p class="open-sans-font">Ketua Devisi Kelembagaan Badan Eksekutif Mahasiswa Fakultas Teknik dan Kejuruan masa bakti 2021/2022</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2021 - 2022</span>
                            <h5 class="poppins-font text-uppercase"> Pengurus UKM KSR-PMI <span class="place open-sans-font">Ganesha University Of Education</span></h5>
                            <p class="open-sans-font"> Sekretaris Bidang 1 UKM KSR-PMI UNIT UNDIKSHA Masa bakti 2021/2022</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019 - Present</span>
                            <h5 class="poppins-font text-uppercase">Student Collage <span class="place open-sans-font">Ganesha University Of Education</span></h5>
                            <p class="open-sans-font">Mahasiswa Program Studi Pendidikan Teknik Informatika Undiksha </p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019 - Present </span>
                            <h5 class="poppins-font text-uppercase">Volunteer <span class="place open-sans-font">PMI</span></h5>
                            <p class="open-sans-font">Relawan KSR-PMI Unit Universitas Pendidikan Ganesha</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Experience & Education Ends -->
    </div>
</section>
<!-- Main Content Ends -->

<!-- Template JS Files -->
<script src="js/jquery-3.5.0.min.js"></script>
<script src="js/styleswitcher.js"></script>
<script src="js/preloader.min.js"></script>
<script src="js/fm.revealator.jquery.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<script src="js/jquery.hoverdir.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>

@endsection
